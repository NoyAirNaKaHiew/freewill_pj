package freewill.chauyarnna.AudioModel

import android.app.Application
import android.app.Dialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.MediaPlayer
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.speech.tts.TextToSpeech
import android.speech.tts.Voice
import android.util.Log
import android.view.*
import android.widget.FrameLayout
import android.widget.SeekBar
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import freewill.chauyarnna.CameraModel.MainCamera
import freewill.chauyarnna.Function.SelectFolderExist
import freewill.chauyarnna.MainActivity
import freewill.chauyarnna.R
import kotlinx.android.synthetic.main.activity_aduio_nonsave_page.*
import kotlinx.android.synthetic.main.activity_aduio_nonsave_page.view.*
import kotlinx.android.synthetic.main.activity_audio_save_page.view.*
import kotlinx.android.synthetic.main.activity_home_page.view.*
import kotlinx.android.synthetic.main.alert_enter_folder.*
import kotlinx.android.synthetic.main.alert_enter_name.*
import kotlinx.android.synthetic.main.alert_enter_name.view.*
import org.apache.commons.io.FileUtils
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.nio.channels.FileChannel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class AduioNonsavePage : Fragment() {
    private var TAG = AduioNonsavePage::class.java.simpleName
    private var speedRate = 0
    private var mStatus = 0;
    private var mMediaPlayer: MediaPlayer? = null
    private var threadHandler = Handler()
    var source: FileChannel? = null
    var destination: FileChannel? = null
    var mProgressDialog: ProgressDialog? = null

    var namefolder = ""
    var selectFolder: Boolean = false
    var dateFormat =  SimpleDateFormat("yyyyMMdd_HH_mm_ss")
    var currentTimeStamp:String = dateFormat.format(Date())
    private var tts: TextToSpeech? = null
    private var voiceobj: Voice? = null
    val texts =
        "ปัจจุบันคุณอยู่หน้าเล่นเสียง จะประกอบไปด้วยปุ่มทั้งหมด 11 ปุ่มอยู่ ตำแหน่งด้านบนมุมซ้ายและขวา 2 ปุ่ม " +
                "และอยู่ตรงกลางหน้าจอกับด้านล่าง 9 ปุ่ม " +
                "เมื่อคุณต้องการบักทึกไฟล์เสียงเพื่อเอาไว้ฟังภายหลังสามารถกดที่ปุ่มด้านล่างสุดของหน้าจอเพื่อทีการบันทึก " +
                "และเมื่อคุณต้องการออกจากหน้านี้ให้ท่านกดที่ปุ่มบนซ้าย และหากต้องการคำแนะนำให้กดที่ปุ่มบนขวา"

    companion object {
        fun newinstant(data: String, check: Boolean) = AduioNonsavePage().apply {
            arguments = Bundle().apply {
                putString("NameFolder", data)
                putBoolean("Check", check)
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        namefolder = arguments?.getString("NameFolder") ?: ""
        selectFolder = arguments?.getBoolean("Check") ?: false
        if (selectFolder == true) {
            createNewfile(namefolder)
            //Toast.makeText(context,namefolder,Toast.LENGTH_LONG).show()
        }

    }

    @Throws(IOException::class)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_aduio_nonsave_page, container, false)

        tts = TextToSpeech(activity, TextToSpeech.OnInitListener { this })
        voiceobj = Voice(
            "it-it-x-kda#male_2-local", Locale.forLanguageTag("th")
            , 1, 1, false, null
        )
        tts!!.voice = voiceobj
        tts!!.setSpeechRate(0.8f)

        view.voice_help_nonsave.setOnClickListener {

            tts!!.speak(texts, TextToSpeech.QUEUE_FLUSH, null, null)

        }

        view.pause_nonsave.visibility = View.GONE
        mMediaPlayer = MediaPlayer()
        view.voice_woman_nonsave.isFocusableInTouchMode = true
        view.voice_woman_nonsave.isFocusable = true


        mProgressDialog = ProgressDialog(context).apply {
            this.setCancelable(false)
            this.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            this.setMessage("Please wait ....")
        }

        view.back_nonsave.setOnClickListener {
            alertBack()
        }

        view.play_nonsave.setOnClickListener {
            view.play_nonsave.visibility = View.GONE
            view.pause_nonsave.visibility = View.VISIBLE
            val myDir: String = Environment.getExternalStorageDirectory().absolutePath + "/sounds/tmpaudio.mp3"
            //   Toast.makeText(activity,myDir,Toast.LENGTH_LONG).show()
            try {
                mMediaPlayer!!.setDataSource(myDir);
                mMediaPlayer!!.prepare();
                mMediaPlayer!!.isLooping = true
            } catch (e: Exception) {

            }
            if (mStatus == 0) {
                doStart()
            } else if (mStatus == 1) {
                doResume()
            }
        }

        view.pause_nonsave.setOnClickListener {
            view.play_nonsave.visibility = View.VISIBLE
            view.pause_nonsave.visibility = View.GONE

            doStop()
        }

        view.mutli_menu_nonsave.setOnClickListener {
            speedRate++
            if (speedRate == 0) {
                view.text_multi_nonsave.text = "1x"
                doSpeed(1f)
            } else if (speedRate == 1) {
                view.text_multi_nonsave.text = "1.50x"
                doSpeed(1.5f)
            } else if (speedRate == 2) {
                view.text_multi_nonsave.text = "1.75x"
                doSpeed(1.75f)
            } else if (speedRate == 3) {
                view.text_multi_nonsave.text = "2x"
                doSpeed(2f)
            } else if (speedRate == 4) {
                view.text_multi_nonsave.text = "1x"
                speedRate = 0
                doSpeed(1f)
            }
        }

        view.refresh_menu_nonsave.setOnClickListener {
            view.play_nonsave.visibility = View.GONE
            view.pause_nonsave.visibility = View.VISIBLE

            doReset()
        }

        view.stop_nonsave.setOnClickListener {
            view.play_nonsave.visibility = View.VISIBLE
            view.pause_nonsave.visibility = View.GONE

            mMediaPlayer!!.pause()
            mMediaPlayer!!.seekTo(0)
            mStatus = 1
        }


        view.voice_man_nonsave.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, p1: MotionEvent?): Boolean {
                v!!.isFocusable = true
                v!!.isFocusableInTouchMode = true
                view.voice_woman_nonsave.isFocusableInTouchMode = false
                view.voice_woman_nonsave.isFocusable = false
                playManSound()
                return false
            }

        })

        view.voice_woman_nonsave.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, p1: MotionEvent?): Boolean {
                v!!.isFocusable = true
                v!!.isFocusableInTouchMode = true
                view.voice_man_nonsave.isFocusableInTouchMode = false
                view.voice_man_nonsave.isFocusable = false
                playWomanSound()
                return false
            }

        })

        view.voice_woman_nonsave.setOnClickListener {
            playWomanSound()
        }
        view.voice_man_nonsave.setOnClickListener {
            playManSound()
        }

        view.fast_forward_nonsave.setOnClickListener { doFastForward() }
        view.fast_rewind_nonsave.setOnClickListener { doRewind() }

        view.btn_save_nonsave.setOnClickListener {
          if(selectFolder != true) {
              val dialog = Dialog(context).apply {
                  this.requestWindowFeature(Window.FEATURE_NO_TITLE)
                  this.setCancelable(false)
                  this.setContentView(R.layout.alert_select_save)
                  this.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
              }
              val mDialogExistFolder = dialog.findViewById<FrameLayout>(R.id.open_directory)
              val mDialogNewFolder = dialog.findViewById<FrameLayout>(R.id.open_newfolder)
              mDialogExistFolder.setOnClickListener {
                  val ft = fragmentManager?.beginTransaction().apply {
                      this?.replace(R.id.frame_header, SelectFolderExist())
                      this?.commit()
                  }
                  dialog.dismiss()
              }
              mDialogNewFolder.setOnClickListener {
                  createNewfolder()
                  dialog.dismiss()
              }
              dialog.show()
          }else if(selectFolder == true){
              createNewfile(namefolder)
          }


        }

        view.seekbar_nonsave.isEnabled = false


        return view
    }

    fun playManSound() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mMediaPlayer!!.isPlaying) {
                mMediaPlayer!!.playbackParams = mMediaPlayer!!.playbackParams.setPitch(0.75f)
            }
        }
    }

    fun playWomanSound() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mMediaPlayer!!.isPlaying) {
                mMediaPlayer!!.playbackParams = mMediaPlayer!!.playbackParams.setPitch(1.0f)
            }
        }
    }

    fun alertBack() {
        val alertDialogBuilder = AlertDialog.Builder(context!!).apply {
            this.setTitle("คุณต้องการกลับไปสู่หน้าหลักหรือไม่ ?")
            this.setPositiveButton("ยืนยัน", object : DialogInterface.OnClickListener {
                override fun onClick(v: DialogInterface?, num: Int) {
                    val intent = Intent(context, MainActivity::class.java)
                    startActivity(intent)
                    activity!!.finish()
                }

            })
            this.setNegativeButton("ยกเลิก", null)

            val alertDialog = this.create()
            alertDialog.show()

        }
    }

    fun createNewfolder() {
        //Toast.makeText(context,currentDateTimeString,Toast.LENGTH_LONG).show()
        val dialog = Dialog(context).apply {
            this.requestWindowFeature(Window.FEATURE_NO_TITLE)
            this.setCancelable(false)
            this.setContentView(R.layout.alert_enter_folder)
            this.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        val Btnsubmit = dialog.findViewById<FrameLayout>(R.id.submit)
        val Btncancel = dialog.findViewById<FrameLayout>(R.id.cancel)
        Btnsubmit.setOnClickListener {
            var name = dialog.name_folder.text
            createNewfile(name.toString())
            dialog.dismiss()
        }
        Btncancel.setOnClickListener {
            Log.d(TAG, "cancel")
            dialog.dismiss()
        }
        dialog.show()
    }

    fun createNewfile(folder: String) {
       /* val dialog = Dialog(context).apply {
            this.requestWindowFeature(Window.FEATURE_NO_TITLE)
            this.setCancelable(false)
            this.setContentView(R.layout.alert_enter_name)
            this.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        val Btnsubmit = dialog.findViewById<FrameLayout>(R.id.submit)
        val Btncancel = dialog.findViewById<FrameLayout>(R.id.cancel)
        Btnsubmit.setOnClickListener {
            mProgressDialog!!.show()
            var name = dialog.name_file.text*/

            //Toast.makeText(context,currentTimeStamp,Toast.LENGTH_LONG).show()
           copyFile(folder,currentTimeStamp)//currentDateTimeString)
           /* dialog.dismiss()


        }
        Btncancel.setOnClickListener {
            Log.d(TAG, "cancel")
            dialog.dismiss()

        }
        dialog.show()*/
    }

    fun doSpeed(speed: Float) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mMediaPlayer!!.isPlaying) {
                mMediaPlayer!!.playbackParams = mMediaPlayer!!.playbackParams.setSpeed(speed)
            }
        }
    }

    private fun millisecondsToString(milliseconds: Int): String {
        var minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds.toLong())
        var seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds.toLong())
        return minutes.toString() + ":" + seconds.toString()
    }

    fun doStart() {
        var duration = this.mMediaPlayer!!.duration

        var currentPosition = this.mMediaPlayer!!.currentPosition
        if (currentPosition == 0) {
            seekbar_nonsave.max = duration
            var maxTimeString = this.millisecondsToString(duration)
            time_end.text = maxTimeString
        } else if (currentPosition == duration) {
            this.mMediaPlayer!!.reset()
        }
        this.mMediaPlayer!!.start()

            var updateSeekBar = object : Runnable {
                override fun run() {
                    try {
                     var currentPosition = mMediaPlayer!!.currentPosition
                     var currentPositionStr = millisecondsToString(currentPosition)
                     time_start.text = currentPositionStr

                     seekbar_nonsave.progress = currentPosition

                     threadHandler.postDelayed(this, 50)
                    }catch (e:Exception){
                       e.printStackTrace()
                    }
                }
            }
            threadHandler.postDelayed(updateSeekBar, 50)

    }

    fun doStop() {
        mMediaPlayer!!.pause()
        mStatus = 1

    }

    fun doResume() {
        var lenght = mMediaPlayer!!.currentPosition
        mMediaPlayer!!.seekTo(lenght)
        mMediaPlayer!!.start()
        mStatus = 0
    }

    fun doReset() {
        mMediaPlayer!!.seekTo(0)
        doStart()
    }


    fun doRewind() {
        var currentPosition = mMediaPlayer!!.currentPosition
        var duration = mMediaPlayer!!.duration

        var SUBTRACT_TIME = 5000
        if (currentPosition - SUBTRACT_TIME > 0) {
            mMediaPlayer!!.seekTo(currentPosition - SUBTRACT_TIME)
        }
    }

    fun doFastForward() {
        var currentPosition = mMediaPlayer!!.currentPosition
        var duration = mMediaPlayer!!.duration

        val ADD_TIME = 5000
        if (currentPosition + ADD_TIME < duration) {
            mMediaPlayer!!.seekTo(currentPosition + ADD_TIME)
        }
    }

    fun copyFile(inFolder: String, inName: String) {
        var sourcePath = Environment.getExternalStorageDirectory().absolutePath + "/sounds/tmpaudio.mp3"
        var source = File(sourcePath)
        var destinationPath = Environment.getExternalStorageDirectory().absolutePath + "/ChauyArnNa_MP3/" + inFolder
        var destination = File(destinationPath)
        if (!destination.exists()) {
            destination.mkdirs()
        }
        Log.d(TAG, destinationPath)
        var tempFilename = inName + ".mp3"
        var tempDstFile = destination.absolutePath + "/" + tempFilename
        var SuccessCopy = File(tempDstFile)
        try {
            FileUtils.copyFile(source, SuccessCopy);
        } catch (e: IOException) {
            e.printStackTrace();
        } finally {
            /* val intent = Intent(context,MainActivity::class.java)
             startActivity(intent)
             activity!!.finish()*/
            mProgressDialog!!.dismiss()
            Toast.makeText(context, "เสร็จสิ้น", Toast.LENGTH_LONG).show()
        }

    }

    override fun onStop() {
        super.onStop()
        try {
            if (mMediaPlayer!!.isPlaying) {
                mMediaPlayer!!.stop()
                mMediaPlayer!!.release()
                mMediaPlayer = null
            } else if (tts != null) {
                tts!!.stop()
                tts!!.shutdown()
            }
        } catch (e: Exception) {

        }
        //Toast.makeText(context,"YO", Toast.LENGTH_SHORT).show()
    }
}

package freewill.chauyarnna.AudioModel

import android.annotation.SuppressLint
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.speech.tts.TextToSpeech
import android.speech.tts.Voice
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import freewill.chauyarnna.MyStorage.AdapterListAudio
import freewill.chauyarnna.MyStorage.ListAudio
import freewill.chauyarnna.R
import kotlinx.android.synthetic.main.activity_audio_save_page.*
import kotlinx.android.synthetic.main.activity_audio_save_page.view.*
import kotlinx.android.synthetic.main.activity_home_page.view.*
import java.io.File
import java.io.IOException
import java.lang.Exception
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class AudioSavePage : Fragment() {
    private var speedRate = 0
    private var mStatus = 0
    private var mMediaPlayer: MediaPlayer? = null
    private var threadHandler = Handler()
    var fileListt: ArrayList<String> = ArrayList()

   // var namefolder = ""
    var namefile = ""
    var position:Int = 0

    private var tts: TextToSpeech? = null
    private var voiceobj: Voice? = null
    val texts = "ปัจจุบันคุณอยู่หน้าเล่นเสียง จะประกอบไปด้วยปุ่มทั้งหมด 12 ปุ่มอยู่ " +
            "ตำแหน่งด้านบนมุมซ้ายและขวา 2 ปุ่ม และอยู่ตรงกลางหน้าจอกับด้านล่าง 10 ปุ่ม " +
            "เมื่อคุณต้องการออกจากหน้านี้ให้ท่านกดที่ปุ่มบนซ้าย และหากต้องการคำแนะนำให้กดที่ปุ่มบนขวา"

    companion object {
        fun newinstant(data: String,position:Int) = AudioSavePage().apply {
            arguments = Bundle().apply {
                //putString("NameFolder", namefile)//file
                putString("NameFile", data)//folder
                putInt("Position",position)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
      //  namefolder = arguments?.getString("NameFolder") ?: ""
        namefile = arguments?.getString("NameFile") ?: ""
        position = arguments?.getInt("Position") ?: 0
        Log.d("CheckValue",namefile+""+position)
    }

    @Throws (IOException::class)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_audio_save_page,container,false)

        tts = TextToSpeech( activity,TextToSpeech.OnInitListener {this})
        voiceobj = Voice("it-it-x-kda#male_2-local", Locale.forLanguageTag("th")
            ,1,1,false,null)
        tts!!.voice = voiceobj
        tts!!.setSpeechRate(0.8f)

        view.voice_help_save.setOnClickListener {

            tts!!.speak(texts,TextToSpeech.QUEUE_FLUSH,null,null)

        }

        view.pause_save.visibility = View.GONE
        mMediaPlayer = MediaPlayer()
        selectStorage()

        view.back_save.setOnClickListener {
            mMediaPlayer!!.stop()
            mMediaPlayer!!.release()
            val ft = fragmentManager?.beginTransaction().apply {
                this?.replace(R.id.container_frame, ListAudio.newinstant(namefile))
                this?.commit()
            }

        }


        view.name_voice_save.text = fileListt[position]

        view.next_skip_save.setOnClickListener {

            Log.d("CheckFile",fileListt[position])
            position += 1
            if(position > fileListt.size-1){
                position = 0
            }
            if(mMediaPlayer!!.isPlaying){
                mMediaPlayer!!.stop()
            }
            val ft = fragmentManager?.beginTransaction().apply {
                this?.replace(R.id.container_frame,newinstant(namefile,position))
                this?.commit()
            }

        }

        view.previous_skip_save.setOnClickListener {
            Log.d("CheckFile",fileListt[position])
            position -= 1
            if(position < 0){
                position = fileListt.size-1
            }
            if(mMediaPlayer!!.isPlaying){
                mMediaPlayer!!.stop()
            }
            val ft = fragmentManager?.beginTransaction().apply {
                this?.replace(R.id.container_frame,newinstant(namefile,position))
                this?.commit()
            }
        }


        view.play_save.setOnClickListener {
            view.play_save.visibility = View.GONE
            view.pause_save.visibility = View.VISIBLE
            //switch between namefile and namefolder
            val myDir =  Environment.getExternalStorageDirectory().absolutePath + "/ChauyArnNa_MP3/"+namefile+"/"+fileListt[position]
         //  Toast.makeText(context,myDir,Toast.LENGTH_LONG).show()
            try{
               mMediaPlayer.apply {
                   this!!.setDataSource(myDir)
                   this!!.prepare()
                   this!!.isLooping = true
               }

            }catch (e:Exception){

            }
            if(mStatus == 0){
                doStart()
            }
            else if(mStatus == 1 ){
                doResume()
            }
        }

        view.pause_save.setOnClickListener {
            view.play_save.visibility = View.VISIBLE
            view.pause_save.visibility = View.GONE

            doStop()
        }

        view.mutli_menu_save.setOnClickListener {
            speedRate++
            if(speedRate == 0){
                view.text_multi.text="1x"
                view.text_multi.textSize = 50F
            }else if(speedRate == 1){
                view.text_multi.text = "1.50x"
                view.text_multi.textSize = 35F

                doSpeed(1.5f)
            }
            else if(speedRate == 2){
                view.text_multi.text = "1.75x"
                view.text_multi.textSize = 35F

                doSpeed(1.75f)
            }
            else if(speedRate == 3){
                view.text_multi.text = "2x"
                view.text_multi.textSize = 35F

                doSpeed(2f)
            }
            else if(speedRate == 4){
                view.text_multi.text = "1x"
                view.text_multi.textSize = 50F

                speedRate = 0
                doSpeed(1f)
            }

        }

        view.refresh_menu_save.setOnClickListener {
            view.play_save.visibility =View.VISIBLE
            view.pause_save.visibility = View.GONE

            doReset()
        }
        view.stop_save.setOnClickListener {
            view.play_save.visibility =View.VISIBLE
            view.pause_save.visibility = View.GONE

            mMediaPlayer.apply {
                this!!.pause()
                this!!.seekTo(0)
                mStatus = 1
            }
        }

        view.voice_man_save.setOnTouchListener(object:View.OnTouchListener{
            override fun onTouch(v: View?, e: MotionEvent?): Boolean {
                v!!.isFocusable = true
                v!!.isFocusableInTouchMode = true
                view.voice_woman_save.isFocusableInTouchMode = false
                view.voice_woman_save.isFocusable = false
                playManSound()
                return false
            }
        })

        view.voice_woman_save.setOnTouchListener(object:View.OnTouchListener{
            override fun onTouch(v: View?, e: MotionEvent?): Boolean {
                v!!.isFocusable = true
                v!!.isFocusableInTouchMode = true
                view.voice_man_save.isFocusableInTouchMode = false
                view.voice_man_save.isFocusable = false
                playWomanSound()
                return false
            }
        })

        view.voice_man_save.setOnClickListener {
           playManSound()
        }
        view.voice_woman_save.setOnClickListener {
            playWomanSound()
        }
        view.fast_forward_save.setOnClickListener {
            doFastForward()
        }
        view.fast_rewind_save.setOnClickListener {
            doRewind()
        }
        view.seekbar_save.isEnabled = false

        return view
    }

    fun playManSound(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mMediaPlayer!!.isPlaying) {
                mMediaPlayer!!.playbackParams = mMediaPlayer!!.playbackParams.setPitch(0.75f)
            }
        }
    }
    fun playWomanSound(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mMediaPlayer!!.isPlaying) {
                mMediaPlayer!!.playbackParams = mMediaPlayer!!.playbackParams.setPitch(1.0f)
            }
        }
    }
    @SuppressLint("WrongConstant")
    fun write(root: File) {
        val listAllFiles = root.listFiles()
        if (listAllFiles.isNotEmpty()) {
            for (currentFile in listAllFiles) {
                fileListt.add(currentFile.name.toString())
            }
        }
    }
    private fun selectStorage() {
        var gpath: String = Environment.getExternalStorageDirectory().absolutePath
        var spath = "ChauyArnNa_MP3/$namefile"
        var fullpath = File(gpath + File.separator + spath)

        Log.d("Yomaneee",fullpath.toString())
        if (namefile.isNotBlank()) {
            write(fullpath)
        } else {
            Log.e("NameFolder","NameFolder = Null")

        }

    }




    fun doSpeed(speed:Float){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(mMediaPlayer!!.isPlaying){
                mMediaPlayer!!.playbackParams = mMediaPlayer!!.playbackParams.setSpeed(speed)
            }
        }
    }

    private fun millisecondsToString(milliseconds:Int):String{
        var minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds.toLong())
        var seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds.toLong())
        return minutes.toString()+":"+ seconds.toString();
    }

    fun doStart(){
        var duration = this.mMediaPlayer!!.duration

        var currentPosition = this.mMediaPlayer!!.currentPosition
        if(currentPosition == 0){
            seekbar_save.max = duration
            var maxTimeString = this.millisecondsToString(duration)
            time_end.text = maxTimeString
        }else if(currentPosition == duration){
            this.mMediaPlayer!!.reset()
        }
        this.mMediaPlayer!!.start()
        var updateSeekBar = object : Runnable {
            override fun run() {
                try {
                    var currentPosition = mMediaPlayer!!.currentPosition
                    var currentPositionStr = millisecondsToString(currentPosition)
                    time_start.text = currentPositionStr

                    seekbar_save.progress = currentPosition

                    threadHandler.postDelayed(this, 50)
                }catch (e:Exception){

                }
            }
        }
        threadHandler.postDelayed(updateSeekBar,50)
    }

    fun doStop(){
        mMediaPlayer!!.pause()
        mStatus = 1

    }
    fun doResume(){
        var lenght = mMediaPlayer!!.currentPosition
        mMediaPlayer!!.seekTo(lenght)
        mMediaPlayer!!.start()
        mStatus = 0
    }

    fun doReset(){
        mMediaPlayer!!.seekTo(0)
        doResume()
    }



    fun doRewind(){
        if(mMediaPlayer!!.isPlaying) {
            var currentPosition = mMediaPlayer!!.currentPosition
            var duration = mMediaPlayer!!.duration

            var SUBTRACT_TIME = 5000
            if (currentPosition - SUBTRACT_TIME > 0) {
                mMediaPlayer!!.seekTo(currentPosition - SUBTRACT_TIME)
            }
        }
    }
    fun doFastForward(){
        if(mMediaPlayer!!.isPlaying) {
            var currentPosition = mMediaPlayer!!.currentPosition
            var duration = mMediaPlayer!!.duration

            val ADD_TIME = 5000
            if (currentPosition + ADD_TIME < duration) {
                mMediaPlayer!!.seekTo(currentPosition + ADD_TIME)
            }
        }
    }
    override fun onStop() {
        super.onStop()
        try {
            if (mMediaPlayer!!.isPlaying) {
                mMediaPlayer!!.stop()
                mMediaPlayer!!.release()
                mMediaPlayer = null
                // Toast.makeText(context, "YO", Toast.LENGTH_SHORT).show()
            }else if(tts != null){
                tts!!.stop()
                tts!!.shutdown()
            }
        }catch (e:Exception){

        }
    }
}

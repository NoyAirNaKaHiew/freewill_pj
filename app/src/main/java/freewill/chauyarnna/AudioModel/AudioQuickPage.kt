package freewill.chauyarnna.AudioModel

import android.annotation.TargetApi
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionText
import freewill.chauyarnna.CameraModel.CameraInterface
import freewill.chauyarnna.R
import kotlinx.android.synthetic.main.activity_audio_quick_page.view.*
import java.lang.ClassCastException
import android.graphics.BitmapFactory
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.Handler
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.widget.ProgressBar
import com.balysv.materialripple.MaterialRippleLayout
import kotlinx.android.synthetic.main.activity_audio_quick_page.*
import org.w3c.dom.Text
import java.io.File
import java.io.IOException
import java.lang.Exception
import java.lang.IllegalArgumentException
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap


class AudioQuickPage : Fragment(){

    private var TAG = AudioQuickPage::class.java.simpleName
    private var speedRate = 0
    private var mStatus = 0;
    private var mMediaPlayer: MediaPlayer? = null
    private var threadHandler = Handler()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_audio_quick_page, container, false)

        view.pause_quick.visibility = View.GONE
        mMediaPlayer = MediaPlayer();

        view.play_quick.setOnClickListener {
            view.play_quick.visibility = View.GONE
            view.pause_quick.visibility = View.VISIBLE
            val myDir: String = Environment.getExternalStorageDirectory().absolutePath + "/sounds/tmpaudio.mp3"
            //   Toast.makeText(activity,myDir,Toast.LENGTH_LONG).show()
            try {
                mMediaPlayer!!.setDataSource(myDir);
                mMediaPlayer!!.prepare();
                mMediaPlayer!!.isLooping = true
            }catch (e:Exception){

            }
            if(mStatus == 0) {
                doStart()
            }
            else if(mStatus == 1){
                doResume()
            }
        }

        view.pause_quick.setOnClickListener {
            view.play_quick.visibility = View.VISIBLE
            view.pause_quick.visibility = View.GONE

             doStop()
        }

        view.mutli_menu_quick.setOnClickListener {
              speedRate++
              if(speedRate == 0){
                  view.text_multi.text = "1x"
                  doSpeed(1f)
              }
              else if(speedRate == 1){
                  view.text_multi.text = "1.50x"
                  doSpeed(1.5f)
              }
              else if(speedRate == 2){
                  view.text_multi.text = "1.75x"
                  doSpeed(1.75f)
              }
              else if(speedRate == 3){
                  view.text_multi.text = "2x"
                  doSpeed(2f)
              }
              else if(speedRate == 4){
                  view.text_multi.text = "1x"
                  speedRate = 0
                  doSpeed(1f)
              }

        }

        view.refresh_menu_quick.setOnClickListener {
          view.play_quick.visibility = View.GONE
          view.pause_quick.visibility = View.VISIBLE

          doReset()
        }
        return view
    }
    fun doSpeed(speed:Float){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
           if(mMediaPlayer!!.isPlaying){
               mMediaPlayer!!.playbackParams = mMediaPlayer!!.playbackParams.setSpeed(speed)
           }
            else{
               mMediaPlayer!!.playbackParams = mMediaPlayer!!.playbackParams.setSpeed(speed)
               mMediaPlayer!!.pause()
           }
        }
    }
    private fun millisecondsToString(milliseconds:Int):String{
        var minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds.toLong())
        var seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds.toLong())
        return minutes.toString()+":"+ seconds.toString();
    }

    fun doStart(){
        var duration = this.mMediaPlayer!!.duration

        var currentPosition = this.mMediaPlayer!!.currentPosition
        if(currentPosition == 0){
            seekbar_quick.max = duration
            var maxTimeString = this.millisecondsToString(duration)
            time_end.text = maxTimeString
        }else if(currentPosition == duration){
            this.mMediaPlayer!!.reset()
        }
        this.mMediaPlayer!!.start()
        var updateSeekBar = object : Runnable {
            override fun run() {
                var currentPosition = mMediaPlayer!!.currentPosition
                var currentPositionStr = millisecondsToString(currentPosition)
                time_start.text = currentPositionStr

                seekbar_quick.progress = currentPosition

                threadHandler.postDelayed(this, 50)
            }
        }
        threadHandler.postDelayed(updateSeekBar,50)
    }
    fun doStop(){
        mMediaPlayer!!.pause()
        mStatus = 1

    }

    fun doResume(){
        var lenght = mMediaPlayer!!.currentPosition
        mMediaPlayer!!.seekTo(lenght)
        mMediaPlayer!!.start()
        mStatus = 0
    }

    fun doReset(){
        mMediaPlayer!!.seekTo(0);
        doStart()
    }

    override fun onDestroy() {
        super.onDestroy()
        if(mMediaPlayer != null){
            mMediaPlayer!!.stop()
            mMediaPlayer!!.release()
            mMediaPlayer = null
        }
    }
}







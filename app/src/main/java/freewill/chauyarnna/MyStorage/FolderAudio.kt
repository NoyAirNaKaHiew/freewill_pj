package freewill.chauyarnna.MyStorage

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.speech.tts.TextToSpeech
import android.speech.tts.Voice
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import freewill.chauyarnna.R
import freewill.chauyarnna.homePage
import kotlinx.android.synthetic.main.activity_folder_audio.*
import kotlinx.android.synthetic.main.activity_folder_audio.view.*
import kotlinx.android.synthetic.main.activity_home_page.view.*
import kotlinx.android.synthetic.main.alert_edit.*
import kotlinx.android.synthetic.main.alert_enter_folder.*
import kotlinx.android.synthetic.main.list_folder.*
import kotlinx.coroutines.*
import java.io.File
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class FolderAudio : Fragment() {
    var TAG = FolderAudio::class.java.simpleName
    var gpath: String = Environment.getExternalStorageDirectory().absolutePath
    var spath = "ChauyArnNa_MP3" ///default/"
    var fullpath = File(gpath + File.separator + spath)
    var mProgressDialog: ProgressDialog? = null

    private var tts: TextToSpeech? = null
    private var voiceobj: Voice? = null
    val texts =
        "ปัจจุบันคุณอยู่หน้าเก็บแฟ้มไฟล์เสียง จะประกอบไปด้วยปุ่มทั้งหมด 3 ปุ่ม และรายการแฟ้ม เมื่อคุณต้องการออกจากหน้านี้ให้กดที่ปุ่มมุมด้านซ้ายบนหน้าจอ และหากต้องการคำแนะนำให้กดที่ปุ่มมุมด้านขวาบนหน้าจอ และหากต้องการสร้างแฟ้มเพิ่มให้กดที่ปุ่มมุมขวาล่างของหน้าจอ"

    companion object {
        const val REQUEST_PERMISSION = 1
    }

    @InternalCoroutinesApi
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_folder_audio, container, false)

        tts = TextToSpeech(context, TextToSpeech.OnInitListener { this })
        voiceobj = Voice(
            "it-it-x-kda#male_2-local", Locale.forLanguageTag("th")
            , 1, 1, false, null
        )
        tts!!.voice = voiceobj
        tts!!.setSpeechRate(0.8f)

        view.IMG_BTN_Folder_Audio_Volume.setOnClickListener {
            tts!!.speak(texts, TextToSpeech.QUEUE_FLUSH, null, null)
        }

        mProgressDialog = ProgressDialog(context).apply {
            this.setCancelable(false)
            this.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            this.setMessage("Please wait ....")
        }

        view.btn_press_back.setOnClickListener {
            val ft = fragmentManager?.beginTransaction().apply {
                this?.replace(R.id.container_frame, homePage())
                this?.commit()
            }
        }

        view.IMG_BTN_Folder_Audio.setOnClickListener {
            createNewfolder()
        }
        return view
    }

    fun createNewfolder() {
        val dialog = Dialog(context).apply {
            this.requestWindowFeature(Window.FEATURE_NO_TITLE)
            this.setContentView(R.layout.alert_enter_folder)
            this.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        val Btnsubmit = dialog.findViewById<FrameLayout>(R.id.submit)
        val Btncancel = dialog.findViewById<FrameLayout>(R.id.cancel)
        Btnsubmit.setOnClickListener {
            var name = dialog.name_folder.text.toString()
            var destinationPath = Environment.getExternalStorageDirectory().absolutePath + "/ChauyArnNa_MP3/" + name
            var destination = File(destinationPath)
            if (!destination.exists()) {
                destination.mkdirs()
            } else {
                Toast.makeText(context, "แฟ้มนี้มีอยู่ในระบบแล้ว", Toast.LENGTH_LONG).show()
            }
            ViewReload()
            dialog.dismiss()
        }
        Btncancel.setOnClickListener {
            Log.d(TAG, "cancel")
            dialog.dismiss()
        }
        dialog.show()
    }

    fun ViewReload() {
        val ft = fragmentManager?.beginTransaction().apply {
            this?.replace(R.id.container_frame, FolderAudio())
            this?.commit()
        }
    }

    override fun onStart() {
        super.onStart()
        if (context?.let { ActivityCompat.checkSelfPermission(it, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            } !== PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_PERMISSION)
        }else {
            write(fullpath)
        }

    }

    override fun onResume() {
        super.onResume()
        if (context?.let { ActivityCompat.checkSelfPermission(it, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            } === PackageManager.PERMISSION_GRANTED) {
            write(fullpath)
        }
    }
    @Throws(IOException::class)
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSION -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                createFolder()

            }
        }
    }

    @Throws(IOException::class)
    @SuppressLint("WrongConstant")
    private fun write(root: File) {
        val fileListt: ArrayList<String> = ArrayList()
        val listAllFiles = root.listFiles()
        if (listAllFiles.isNotEmpty()) {
            for (currentFile in listAllFiles) {
                fileListt.add(currentFile.name)
            }
//            Log.d("YOman",fileListt[1])
            val aDap = AdapterFolderAudio(fileListt, context!!)
            aDap.onItemClick = ::onclick
            recycle_Folder_Audio.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
            recycle_Folder_Audio.adapter = aDap
        }

    }

    private fun onclick(data: String, numOn: String, status: String, namefolder: String) {
        //Log.e("+++++++++++++++", data)
        if (numOn == "1") {
            val ft = fragmentManager?.beginTransaction().apply {
                this?.replace(R.id.container_frame, ListAudio.newinstant(data))
                this?.commit()
            }
        } else if (numOn == "2") {

            Log.e("Delete", data + "To Delete_Folder")
            Log.e("numOn =", numOn)
            Log.e("status =", status)
            var fullpath = File(Environment.getExternalStorageDirectory().absolutePath + "/ChauyArnNa_MP3/$data")
            DeleteRecursive(fullpath)

        } else if (numOn == "3") {
            Log.e("Edit", data + "To EditFolder")
            Log.e("numOn =", numOn)
            Log.e("status =", status)
            EditRename(data)
        }
    }

    private fun DeleteRecursive(dir: File) {
        Log.d("DeleteRecursive", "DELETEPREVIOUS TOP" + dir.path)
        if (dir.isDirectory) {
            val children = dir.list()
            for (i in children.indices) {
                val temp = File(dir, children[i])
                if (temp.isDirectory) {
                    Log.d("DeleteRecursive", "Recursive Call" + temp.path)
                    DeleteRecursive(temp)
                } else {
                    Log.d("DeleteRecursive", "Delete File" + temp.path)
                    val b = temp.delete()
                    if (!b) {
                        Log.d("DeleteRecursive", "DELETE FAIL")
                    }
                }
            }
        }
        dir.delete()
        ViewReload()

    }

    private fun EditRename(data: String) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.alert_edit)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.text_edit.setText(data)
        val mDialogSave = dialog.findViewById<FrameLayout>(R.id.btn_save)
        mDialogSave.setOnClickListener {
            var editText = dialog.text_edit.text.toString()
            var oldName = "/ChauyArnNa_MP3/$data"
            var newName = "/ChauyArnNa_MP3/$editText"
            renameFile(oldName, newName)
            Log.e("dataIntextEdit", editText)
            ViewReload()

            dialog.dismiss()
        }
        val mDialogCancel = dialog.findViewById<FrameLayout>(R.id.btn_cancel)
        mDialogCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }
    fun createFolder(){
        var destinationPath = Environment.getExternalStorageDirectory().absolutePath + "/ChauyArnNa_MP3/default/"
        var destination = File(destinationPath)
        if(!destination.exists()){
            destination.mkdirs()
        }
    }
    fun renameFile(oldName: String, newName: String) {
        val dir = Environment.getExternalStorageDirectory()
        if (dir.exists()) {
            val from = File(dir, oldName)
            val to = File(dir, newName)
            if (from.exists())
                from.renameTo(to)
        }
    }

    override fun onStop() {
        super.onStop()
        try {
            if (tts != null) {
                tts!!.stop()
                tts!!.shutdown()
            }
        } catch (e: Exception) {

        }

    }

}

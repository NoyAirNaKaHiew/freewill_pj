package freewill.chauyarnna.MyStorage

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.preference.PreferenceManager
import android.speech.tts.TextToSpeech
import android.speech.tts.Voice
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import freewill.chauyarnna.AudioModel.AudioSavePage
import freewill.chauyarnna.CameraModel.MainCamera
import freewill.chauyarnna.R
import kotlinx.android.synthetic.main.activity_folder_audio.view.*

import kotlinx.android.synthetic.main.activity_list_audio.*
import kotlinx.android.synthetic.main.activity_list_audio.view.*
import kotlinx.android.synthetic.main.activity_list_audio.view.btn_press_back
import kotlinx.android.synthetic.main.alert_edit.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


class ListAudio : Fragment() {

    var namefolder = ""
    var fileListt: ArrayList<String> = ArrayList()
    private val TAG_FRAGMENT = "AudioSave"

    private var tts: TextToSpeech? = null
    private var voiceobj: Voice? = null
    val texts = "ปัจจุบันคุณอยู่หน้าเก็บไฟล์เสียงของแฟ้มชื่อ จะประกอบไปด้วยปุ่มทั้งหมด 3 ปุ่ม และรายการไฟล์เสียง " +
            "เมื่อคุณต้องการออกจากหน้านี้ให้กดที่ปุ่มมุมด้านซ้ายบนหน้าจอ " +
            "และหากต้องการคำแนะนำให้กดที่ปุ่มมุมด้านขวาบนหน้าจอ และหากต้องการสร้างไฟล์เสียงเพิ่มในแฟ้มชื่อ " +
            "ให้กดที่ปุ่มมุมขวาล่างของหน้าจอ"


    companion object {
        fun newinstant(data: String) = ListAudio().apply {
            arguments = Bundle().apply {
                putString("NameFolder", data)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        namefolder = arguments?.getString("NameFolder") ?: ""


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_list_audio, container, false)

        tts = TextToSpeech( activity,TextToSpeech.OnInitListener {this})
        voiceobj = Voice("it-it-x-kda#male_2-local", Locale.forLanguageTag("th")
            ,1,1,false,null)
        tts!!.voice = voiceobj
        tts!!.setSpeechRate(0.8f)

        view.IMG_BTN_List_Audio_Volume.setOnClickListener {

            tts!!.speak(texts,TextToSpeech.QUEUE_FLUSH,null,null)

        }
        view.store_name.text = namefolder
        view.store_name.textSize = 28f

        view.btn_press_back.setOnClickListener {
            val ft = fragmentManager?.beginTransaction().apply {
                this?.replace(R.id.container_frame,FolderAudio())
                this?.commit()
            }
        }

        view.IMG_BTN_LIST_Audio.setOnClickListener {
            val intent = Intent(activity,MainCamera::class.java)
            intent.putExtra("FileCamera",namefolder)
            startActivity(intent)
        }
        return view
    }

    override fun onStart() {
        super.onStart()
        if (context?.let {
                ActivityCompat.checkSelfPermission(
                    it,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            } !== PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), FolderAudio.REQUEST_PERMISSION)
        } else {
            selectStorage()
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            FolderAudio.REQUEST_PERMISSION -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectStorage()
            }
        }
    }

    @SuppressLint("WrongConstant")
    fun write(root: File) {
        val listAllFiles = root.listFiles()
        if (listAllFiles.isNotEmpty()) {
            for (currentFile in listAllFiles) {
                fileListt.add(currentFile.name.toString())
            }

            val adap = AdapterListAudio(fileListt, context!!)
            adap.onItemClick = ::onClick
            recycle_List_Audio.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
            recycle_List_Audio.adapter = adap

        }
    }


    private fun onClick(data: String,position:Int,numOn:String) {
        Log.e("+++++++++++++++",fileListt[position])
        if(numOn == "1") {
            val ft = fragmentManager?.beginTransaction().apply {
                this?.replace(R.id.container_frame,AudioSavePage.newinstant(namefolder,position),TAG_FRAGMENT)
                this?.addToBackStack(null)
                this?.commit()
            }
        }else if (numOn == "2") {


            var fullpath = File(Environment.getExternalStorageDirectory().absolutePath + "/ChauyArnNa_MP3/$namefolder/$data")
            DeleteRecursive(fullpath)

        } else if (numOn == "3"){

            EditRename(data)
        }

    }
    fun ViewReload(){
        val ft = fragmentManager?.beginTransaction().apply {
            this?.replace(R.id.container_frame, newinstant(namefolder))
            this?.commit()
        }
    }
    private fun selectStorage() {
        var gpath: String = Environment.getExternalStorageDirectory().absolutePath
        var spath = "ChauyArnNa_MP3/$namefolder"
        var fullpath = File(gpath + File.separator + spath)

        if (namefolder.isNotBlank()) {
            write(fullpath)
        } else {
            Log.e("NameFolder","NameFolder = Null")

        }

    }
    private fun DeleteRecursive(dir: File) {
        Log.d("DeleteRecursive", "DELETEPREVIOUS TOP" + dir.path)
        if (dir.isDirectory) {
            val children = dir.list()
            for (i in children.indices) {
                val temp = File(dir, children[i])
                if (temp.isDirectory) {
                    Log.d("DeleteRecursive", "Recursive Call" + temp.path)
                    DeleteRecursive(temp)
                } else {
                    Log.d("DeleteRecursive", "Delete File" + temp.path)
                    val b = temp.delete()
                    if (!b) {
                        Log.d("DeleteRecursive", "DELETE FAIL")
                    }
                }
            }
        }
        dir.delete()
        ViewReload()

    }
    private fun EditRename(data: String){
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.alert_edit)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.text_edit.setText(data)
        val mDialogSave = dialog.findViewById<FrameLayout>(R.id.btn_save)
        mDialogSave.setOnClickListener {
            var editText = dialog.text_edit.text.toString()
            var oldName = "/ChauyArnNa_MP3/$namefolder/$data"
            var newName = "/ChauyArnNa_MP3/$namefolder/$editText"
            renameFile(oldName, newName)
            Log.e("dataIntextEdit", editText)
            ViewReload()

            dialog.dismiss()
        }
        val mDialogCancel = dialog.findViewById<FrameLayout>(R.id.btn_cancel)
        mDialogCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }
    fun renameFile(oldName: String, newName: String) {
        val dir = Environment.getExternalStorageDirectory()
        if (dir.exists()) {
            val from = File(dir, oldName)
            val to = File(dir, newName)
            if (from.exists())
                from.renameTo(to)
        }
    }
}

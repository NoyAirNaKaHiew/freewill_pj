package freewill.chauyarnna.MyStorage

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import freewill.chauyarnna.R
import kotlinx.android.synthetic.main.list_folder.view.*
import kotlinx.android.synthetic.main.list_folder.view.title_name

class AdapterListAudio(val items: ArrayList<String>, val context: Context) :
    RecyclerView.Adapter<ViewHolderListAudio>() {

    var onItemClick: ((data: String,position:Int,num:String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderListAudio {

        return ViewHolderListAudio(LayoutInflater.from(context!!).inflate(R.layout.list_folder, parent, false))

    }
    override fun getItemCount(): Int {
        return items.size
    }
    override fun onBindViewHolder(holder: ViewHolderListAudio, position: Int) {
        val data = items[position]
        holder.title.text = data
        var numOn =""


        holder.itemView.setOnClickListener {
            numOn = "1"
            onItemClick?.invoke(data,position,numOn)

        }
        holder.itemView.setOnLongClickListener{
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.alert_if_longclick)
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val mDialogDelete = dialog.findViewById<FrameLayout>(R.id.btn_delete)
            mDialogDelete.setOnClickListener {
                numOn = "2"
                onItemClick?.invoke(data,position,numOn)
                dialog.dismiss()
            }
            val mDialogCancel = dialog.findViewById<FrameLayout>(R.id.btn_edit)
            mDialogCancel.setOnClickListener {
                numOn = "3"
                onItemClick?.invoke(data,position,numOn)
                dialog.dismiss()
            }
            dialog.show()
            return@setOnLongClickListener true
        }
    }
}

class ViewHolderListAudio(view: View) : RecyclerView.ViewHolder(view) {
    val title = view.title_name

}

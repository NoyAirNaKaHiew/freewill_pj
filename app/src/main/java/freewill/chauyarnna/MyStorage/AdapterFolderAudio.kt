package freewill.chauyarnna.MyStorage

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.FrameLayout
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import freewill.chauyarnna.R
import kotlinx.android.synthetic.main.list_folder.view.*

class AdapterFolderAudio(val items: ArrayList<String>, val context: Context) :
    RecyclerView.Adapter<ViewHolderFolderAudio>() {

    var onItemClick: ((data: String,numOn:String,status:String,namefolder:String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderFolderAudio {
        return ViewHolderFolderAudio(LayoutInflater.from(context).inflate(R.layout.list_folder, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolderFolderAudio, position: Int) {
        val data = items[position]
        holder.title.text = data
        var numOn =""
        var  namefolder =""
        var  status="Folder"
       /* if (holder.check.visibility == VISIBLE){
            holder.itemView.setOnClickListener{
              //  holder.check.visibility = GONE

            }
        }*/

        holder.itemView.setOnClickListener{
            //Toast.makeText(context,position.toString(),Toast.LENGTH_LONG).show()
            numOn = "1"

            onItemClick?.invoke(data,numOn,status,namefolder)

     }
        holder.itemView.setOnLongClickListener{
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.alert_if_longclick)
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val mDialogDelete = dialog.findViewById<FrameLayout>(R.id.btn_delete)
            mDialogDelete.setOnClickListener {
                numOn = "2"
                onItemClick?.invoke(data,numOn,status,namefolder)
                dialog.dismiss()
            }
            val mDialogCancel = dialog.findViewById<FrameLayout>(R.id.btn_edit)
            mDialogCancel.setOnClickListener {
                numOn = "3"
                onItemClick?.invoke(data,numOn,status,namefolder)
                dialog.dismiss()
            }
            dialog.show()
            return@setOnLongClickListener true
        }
    }

}
class ViewHolderFolderAudio(view: View) : RecyclerView.ViewHolder(view) {
    val title = itemView.title_name

}
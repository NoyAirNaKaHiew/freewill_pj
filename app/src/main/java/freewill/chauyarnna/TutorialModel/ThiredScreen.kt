package freewill.chauyarnna.TutorialModel

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.speech.tts.Voice
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import freewill.chauyarnna.MainActivity
import freewill.chauyarnna.R
import freewill.chauyarnna.homePage
import kotlinx.android.synthetic.main.activity_thired_screen.*
import kotlinx.android.synthetic.main.activity_thired_screen.view.*
import kotlinx.android.synthetic.main.activity_thired_screen.view.volume_btn

class ThiredScreen :Fragment() {
    private var tts: TextToSpeech? = null
    private var voiceobj: Voice? = null
    val texts = "เราจะมายังหน้าเล่นไฟล์เสียง เเละเรายงสามารถกดบันทึกไฟล์เสียงได้"
    var count:Int? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_thired_screen, container, false)
        view.previous_tutorial.setOnClickListener {
            val transaction = fragmentManager!!.beginTransaction().apply {
                this!!.replace(R.id.container_frame, SecondScreen())
                this!!.addToBackStack(null)
                this!!.commit()
            }
        }
        view.skip_tutorial.setOnClickListener {
            alertBack()
        }

        view.next_tutorial.setOnClickListener {
            val transaction = fragmentManager!!.beginTransaction().apply {
                this!!.replace(R.id.container_frame,homePage())
                this!!.addToBackStack(null)
                this!!.commit()
            }
        }


        return view
    }
    fun alertBack(){
        val alertDialogBuilder = AlertDialog.Builder(context!!).apply{
            this.setTitle("คุณต้องการกลับไปสู่หน้าหลักหรือไม่ ?")
            this.setPositiveButton("ยืนยัน",object : DialogInterface.OnClickListener{
                override fun onClick(v: DialogInterface?, num: Int) {
                    val intent = Intent(context, MainActivity::class.java)
                    startActivity(intent)
                    activity!!.finish()
                }

            })
            this.setNegativeButton("ยกเลิก",null)

            val alertDialog = this.create()
            alertDialog.show()

        }
    }

    override fun onStart() {
        super.onStart()
        volume_btn.setOnClickListener {
            speak()
        }
        speak()

    }
    fun speak(){
        tts = TextToSpeech(activity, TextToSpeech.OnInitListener { status ->
            if (status != TextToSpeech.ERROR) {
                tts!!.setSpeechRate(0.8f)
                tts!!.setPitch(1.0f)
                tts!!.speak(texts, TextToSpeech.QUEUE_FLUSH, null)
            }

        })

    }
    override fun onStop() {
        super.onStop()
        try {
            if (tts != null) {
                tts!!.stop()
                tts!!.shutdown()
            }
        } catch (e: Exception) {

        }

    }
}

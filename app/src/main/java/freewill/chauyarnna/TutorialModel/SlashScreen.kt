package freewill.chauyarnna.TutorialModel

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat.finishAffinity
import androidx.fragment.app.Fragment
import freewill.chauyarnna.MainActivity
import freewill.chauyarnna.R

class SlashScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        setContentView(R.layout.activity_main )
        Handler().postDelayed({
            startActivity(Intent(this, MainActivity::class.java))
           // finishAffinity()
            finish()
        }, 2000)
    }

}

package freewill.chauyarnna

import android.Manifest
import android.annotation.TargetApi
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.MediaPlayer
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.speech.tts.TextToSpeech
import android.view.Window
import android.widget.FrameLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.fragment.app.FragmentManager
import freewill.chauyarnna.AudioModel.AudioQuickPage
import freewill.chauyarnna.AudioModel.AudioSavePage
import freewill.chauyarnna.CameraModel.CameraInterface
import freewill.chauyarnna.CameraModel.MainCamera
import freewill.chauyarnna.MyStorage.FolderAudio
import freewill.chauyarnna.TutorialModel.FirstScreen
import java.io.File
import java.io.IOException
import java.util.*

class MainActivity : AppCompatActivity(),HomeInterface{

    private val TAG_FRAGMENT = "AudioSave";
    private lateinit var fragment: FragmentManager
    private var onTake: CameraInterface? = null

    companion object {
        const val REQUEST_PERMISSION = 1
        const val REQUEST_CAMERA = 2;
    }

    @Throws(IOException::class)
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fragment = supportFragmentManager


        onPageChange(0)
    }
    fun onPageChange(page:Int){
        if(page == 0){
            val transaction = fragment.beginTransaction()
            transaction.replace(R.id.container_frame,homePage())
            transaction.addToBackStack(null);
            transaction.commit()
        }
        else if(page == 1){
             //DialogChoose()
           val intent = Intent(this,MainCamera::class.java)
            intent.putExtra("FileCamera","")
            startActivity(intent)
        }
        else if(page == 2){
            val transaction = fragment.beginTransaction()
            transaction.replace(R.id.container_frame,FolderAudio())
            transaction.addToBackStack(null);
            transaction.commit()

        }
        else if(page == 3){
            val transaction = fragment.beginTransaction()
            transaction.replace(R.id.container_frame,FirstScreen())
            transaction.addToBackStack(null);
            transaction.commit()
        }
    }
    override fun onPage(page: Int) {
        onPageChange(page)
    }



    fun createFolder(){
        var destinationPath = Environment.getExternalStorageDirectory().absolutePath + "/ChauyArnNa_MP3/default/"
        var destination = File(destinationPath)
        if(!destination.exists()){
            destination.mkdirs()
        }
    }

    override fun onBackPressed() {

    }
    @Throws(IOException::class)
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSION -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                createFolder()

            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    override fun onStart() {
        super.onStart()
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
             !== PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
            !== PackageManager.PERMISSION_GRANTED ) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_PERMISSION)
            requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA)

        } else {
           createFolder()
        }

    }
   /*override fun onStop() {
        super.onStop()
        val mMediaPlayer = MediaPlayer()
        mMediaPlayer.stop()
        mMediaPlayer.release()
    }*/
}

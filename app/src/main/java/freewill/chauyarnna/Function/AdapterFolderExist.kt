package freewill.chauyarnna.Function

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import freewill.chauyarnna.R
import kotlinx.android.synthetic.main.list_folder.view.*

class AdapterFolderExist(val items: ArrayList<String>, val context: Context) :
    RecyclerView.Adapter<ViewHolderFolderAudio>() {

    var onItemClick: ((data: String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderFolderAudio {
        return ViewHolderFolderAudio(LayoutInflater.from(context).inflate(R.layout.list_folder, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolderFolderAudio, position: Int) {
        val data = items[position]
        holder.title.text = data

        /* if (holder.check.visibility == VISIBLE){
             holder.itemView.setOnClickListener{
               //  holder.check.visibility = GONE

             }
         }*/

        holder.itemView.setOnClickListener{
            onItemClick?.invoke(data)

        }
    }

}
class ViewHolderFolderAudio(view: View) : RecyclerView.ViewHolder(view) {
    val title = itemView.title_name

}
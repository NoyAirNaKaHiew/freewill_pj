package freewill.chauyarnna.Function

class MapComparator:Comparator<HashMap<String,String>>{
    var key:String = ""
    var order:String = ""

    fun MapComparator(key:String,order:String){
         this.key = key
         this.order = order
    }
    override fun compare(first: java.util.HashMap<String, String>?, second: java.util.HashMap<String, String>?): Int {
         var firstValue = first!!.get(key)
         var secondValue = second!!.get(key)
         if(this.order.toLowerCase().contentEquals("asc")){
             return firstValue!!.compareTo(secondValue.toString())
         }else{
             return secondValue!!.compareTo(firstValue.toString())
         }

    }

}
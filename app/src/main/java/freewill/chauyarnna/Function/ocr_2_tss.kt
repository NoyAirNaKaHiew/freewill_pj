package freewill.chauyarnna.Function

import android.app.Activity
import android.graphics.Bitmap
import android.os.Environment
import android.os.Handler
import android.speech.tts.TextToSpeech
import android.util.Log
import android.widget.Toast
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionText
import java.io.File

/*class ocr_2_tss:Activity(){
    private var threadHandler = Handler()
    private var mTTS: TextToSpeech? = null
    private var TAG = ocr_2_tss::class.java.simpleName

    /** OCR **/
    fun detectText(bitmap: Bitmap){
        val image = FirebaseVisionImage.fromBitmap(bitmap)
        val detector = FirebaseVision.getInstance()
            .onDeviceTextRecognizer
        val result = detector.processImage(image)
            .addOnSuccessListener { firebaseVisionText ->
                processText(firebaseVisionText)
            }
            .addOnFailureListener {
            }
    }
    fun processText(firebaseVisionText: FirebaseVisionText){
        val resultText =firebaseVisionText.text
        if(resultText.equals("")){
            Log.d(TAG,"Not have text")
            mProgressDialog!!.dismiss()
            Toast.makeText(this,"ไม่พบข้อความบนรูปภาพ", Toast.LENGTH_LONG).show()
        }
        else if(!resultText.equals("")){
            Log.d(TAG,"Converted")

            if(CameraCheck == 1){

            }
            else if(CameraCheck == 2){
                TTS_camera_instance(resultText)
            }

        }

    }

    private fun TTS_camera_instance(result:String){
        var myHashRender = HashMap<String,String>()
        myHashRender.put(
            TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,
            result)
        val myDir: String = Environment.getExternalStorageDirectory().absolutePath +  "/sounds/"
        val appTmpPath = File(myDir)
        if(!appTmpPath.exists()) {
            appTmpPath.mkdirs()
        }
        Log.d(TAG,myDir)
        //Toast.makeText(activity,myDir,Toast.LENGTH_LONG).show()
        var tempFilename = "tmpaudio.mp3"
        var tempDestFile = appTmpPath.absolutePath+"/"+tempFilename


        var show =  mTTS!!.synthesizeToFile(result,myHashRender,tempDestFile)
        if(show == TextToSpeech.SUCCESS){
            var uploadFile = object : Runnable {
                override fun run() {
                    threadHandler.postDelayed(this, 1500)
                }
            }
            threadHandler.postDelayed(uploadFile,1500)
            playMedia(0)

        }
    }
}*/
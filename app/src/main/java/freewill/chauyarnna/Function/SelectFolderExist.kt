package freewill.chauyarnna.Function

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import freewill.chauyarnna.AudioModel.AduioNonsavePage
import freewill.chauyarnna.R
import freewill.chauyarnna.homePage
import kotlinx.android.synthetic.main.activity_folder_audio.*
import kotlinx.android.synthetic.main.activity_select_folder_exist.*
import kotlinx.android.synthetic.main.activity_select_folder_exist.view.*
import kotlinx.android.synthetic.main.list_folder.*
import kotlinx.coroutines.*
import java.io.File

class SelectFolderExist : Fragment() {

    var gpath: String = Environment.getExternalStorageDirectory().absolutePath
    var spath = "ChauyArnNa_MP3" ///default/"
    var fullpath = File(gpath +File.separator+ spath)

    companion object {
        const val REQUEST_PERMISSION = 1
    }

    @InternalCoroutinesApi
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_select_folder_exist,container,false)
        view.btn_press_back.setOnClickListener {
            val ft = fragmentManager?.beginTransaction().apply {
                this?.replace(R.id.frame_header,AduioNonsavePage())
                this?.commit()
            }
        }
        return view
    }
    override fun onStart() {
        super.onStart()
        if (context?.let { ActivityCompat.checkSelfPermission(it, Manifest.permission.WRITE_EXTERNAL_STORAGE) } !== PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_PERMISSION)
        } else {
            write(fullpath)
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSION -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                write(fullpath)
            }
        }
    }

    @SuppressLint("WrongConstant")
    private fun write(root: File) {
        val fileListt: ArrayList<String> = ArrayList()
        val listAllFiles = root.listFiles()
        if (listAllFiles.isNotEmpty()) {
            for (currentFile in listAllFiles) {
                fileListt.add(currentFile.name)
            }
            val aDap = AdapterFolderExist(fileListt, context!!)
            aDap.onItemClick = ::onclick
            recycle_List_Audio_select.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
            recycle_List_Audio_select.adapter = aDap
        }

    }

    private fun onclick(data: String) {
        Log.e("+++++++++++++++",data)
        val ft = fragmentManager?.beginTransaction().apply {
            this?.replace(R.id.frame_header,AduioNonsavePage.newinstant(data,true))
            this?.commit()
        }
    }
}

package freewill.chauyarnna.Function

import android.content.Context
import android.content.pm.PackageManager
import android.database.Cursor
import android.database.MergeCursor
import android.os.Build
import android.provider.MediaStore
import androidx.core.app.ActivityCompat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class GalleryFunction{
    val KEY_ALBUM = "album_name"
    val KEY_PATH = "path"
    val KEY_TIMESTAMP = "timestamp"
    val KEY_TIME = "date"
    val KEY_COUNT = "date"

    fun hasPermissions(context: Context,permissions:String):Boolean{
        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null){
            for(permission in permissions){
                if(ActivityCompat.checkSelfPermission(context, permission.toString()) != PackageManager.PERMISSION_GRANTED){
                    return false
                }
            }
        }
        return true
    }

    fun mappingInbox(album:String,path:String,timestamp:String,time:String,count:String):HashMap<String,String>{
        val map = HashMap<String,String>()
        map.put(KEY_ALBUM,album)
        map.put(KEY_PATH,path)
        map.put(KEY_TIMESTAMP,timestamp)
        map.put(KEY_TIME,time)
        map.put(KEY_COUNT,count)
        return map
    }

    fun getCount(c:Context,album_name:String):String{
        val uriExternal = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val uriInternal = android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI
        val projection = arrayOf(
            MediaStore.MediaColumns.DATA,
            MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
            MediaStore.MediaColumns.DATE_MODIFIED
        )
        val cursorExternal = c.contentResolver.query(uriExternal,projection,"bucket_display_name = \""+album_name+"\"", null, null)
        val cursorInternal = c.contentResolver.query(uriInternal, projection, "bucket_display_name = \""+album_name+"\"", null, null)

        val cursor = MergeCursor(arrayOf(cursorExternal, cursorInternal))

        return  cursor.count.toString()+" Photos"
    }

    fun converToTime(timestamp: String):String{
        var datetime = timestamp.toLong()
        val date = Date(datetime)
        val formatter = SimpleDateFormat("dd/MM HH:mm")
        return formatter.format(date)
    }

    fun convertDpToPixel(dp:Float,contexts:Context):Float{
        val resources = contexts.resources
        val metrics = resources.displayMetrics
        var px = dp*(metrics.densityDpi/160f)
        return px
    }
}

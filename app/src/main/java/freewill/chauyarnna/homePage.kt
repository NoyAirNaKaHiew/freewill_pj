package freewill.chauyarnna

import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.speech.tts.Voice
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import freewill.chauyarnna.AudioModel.AduioNonsavePage
import freewill.chauyarnna.CameraModel.CameraInterface
import freewill.chauyarnna.CameraModel.MainCamera
import kotlinx.android.synthetic.main.activity_home_page.*
import kotlinx.android.synthetic.main.activity_home_page.view.*
import java.lang.ClassCastException
import java.util.*

class homePage : Fragment() {
    private var onTake: HomeInterface? = null
    private var tts: TextToSpeech? = null
    private var voiceobj: Voice? = null
    val texts =
        "ปัจจุบันคุณอยู่ที่หน้า โฮมซึ่งจะประกอบไปด้วยปุ่ม 3 ปุ่มจะอยู่ตำแหน่งตรงกลางหน้าจอ ถ้าหากคุณเข้าสู่โหมดการเข้าถึงสำหรับคนตาบอดในโทรศัพท์ตัวปุ่มจะทำการบอกชื่อของปุ่มที่คุณสัมผัส"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_home_page, container, false)

        tts = TextToSpeech(activity, TextToSpeech.OnInitListener { this })
        voiceobj = Voice(
            "it-it-x-kda#male_2-local", Locale.forLanguageTag("th")
            , 1, 1, false, null
        )
        tts!!.voice = voiceobj
        tts!!.setSpeechRate(0.8f)

        view.volume_btn.setOnClickListener {

            tts!!.speak(texts, TextToSpeech.QUEUE_FLUSH, null, null)

        }

        view.main_read_ocr.setOnClickListener {
            onTake!!.onPage(1)
        }
        view.main_storage.setOnClickListener {
            onTake!!.onPage(2)

        }
        view.main_help.setOnClickListener {
            onTake!!.onPage(3)

        }

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            onTake = context as HomeInterface

        } catch (e: ClassCastException) {
            throw ClassCastException(
                context.toString()
                        + " must implement OnFragmentInteractionListener"
            );
        }
    }

    override fun onDetach() {
        super.onDetach()
        onTake = null
    }

    override fun onStop() {
        super.onStop()
        try {
            if (tts != null) {
                tts!!.stop()
                tts!!.shutdown()
            }
        } catch (e: Exception) {

        }

    }
}

package freewill.chauyarnna.CameraModel

import android.Manifest
import android.annotation.TargetApi
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.os.*
import android.preference.PreferenceManager
import android.provider.Contacts
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.text.format.Time
import android.util.Log
import android.util.Rational
import android.util.Size
import android.view.LayoutInflater
import android.view.Surface
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.FragmentManager
import freewill.chauyarnna.R
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.camera.core.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionText
import freewill.chauyarnna.AudioModel.AduioNonsavePage
import freewill.chauyarnna.AudioModel.AudioQuickPage
import freewill.chauyarnna.MainActivity
import kotlinx.android.synthetic.main.activity_main_camera.*
import kotlinx.coroutines.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.nio.ByteBuffer
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap
import kotlin.coroutines.suspendCoroutine

private const val REQUEST_CODE_PERMISSIONS = 10
private const val MAX_PREVIEW_WIDTH = 1920
private const val MAX_PREVIEW_HEIGHT = 1080

private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)

class MainCamera : AppCompatActivity(),CameraInterface{

    var bitmap: Bitmap? = null
    private lateinit var fragment:FragmentManager
    private var countPage:Int = 2
    private var CameraCheck:Int = 0
    private var TAG = MainCamera::class.java.simpleName
    var imageCapture:ImageCapture? = null
    private var mTTS: TextToSpeech? = null
    private var mProgressDialog:ProgressDialog? = null
    private var today:Time? = null
    private var myHashRender:HashMap<String,String>? = null
    var tempFilename:String = ""
    var tempDestFile:String = ""
    var dateTime:String = ""
    var FileCamera:String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_camera)
        FileCamera = intent.getStringExtra("FileCamera")
        fragment = supportFragmentManager


        mProgressDialog = ProgressDialog(this).apply {
            this.setCancelable(false)
            this.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            this.setMessage("Please wait ...")
        }

        TTS_fun()

        createFolder()

        today = Time(Time.getCurrentTimezone())
        today!!.setToNow()
        dateTime = today!!.monthDay.toString()+"_"+today!!.month.toString()+"_"+today!!.year.toString()+"_"+today!!.format("%k_%M_%S")

        //Toast.makeText(this,today!!.monthDay.toString(),Toast.LENGTH_LONG).show()



        if(allPermissionsGranted()){
            view_finder.post { startCamera() }
        }
        else{
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
        }

        view_finder.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
            updateTransform()
        }

     /*   val audioPage = AduioNonsavePage()
        val transaction = fragment.beginTransaction().apply {
            container.isVisible = false
            this.setCustomAnimations(R.animator.enter,R.animator.pop_enter)
            this.replace(R.id.frame_header,audioPage,"AudioNonsavePage")
            this.addToBackStack(null)
            this.commit()
        }*/

        //Change Camera Page
        onFragmentChanged(countPage)

        //Switch Camera
        /*camera_instant_switch.setOnClickListener {
            onFragmentChanged(countPage)
        }*/
    }

    override fun onBackPressed() {

    }
    fun onFragmentChanged(pageId:Int){
       if(pageId == 1){
          /* Toast.makeText(this,R.string.default_camera,Toast.LENGTH_LONG).show()
           val transaction = fragment.beginTransaction()
           transaction.setCustomAnimations(R.animator.card_flip_right_in, R.animator.card_flip_right_out
           ,R.animator.card_flip_left_in,R.animator.card_flip_left_out)
           transaction.replace(R.id.container,CameraPage())
           transaction.addToBackStack(null);
           transaction.commit()

           countPage = 2*/
       }
       if(pageId == 2){
           val transaction = fragment.beginTransaction().apply {
              // this.setCustomAnimations(R.animator.card_flip_right_in, R.animator.card_flip_right_out
                  // ,R.animator.card_flip_left_in,R.animator.card_flip_left_out)
               this.replace(R.id.container,CameraQuickPage())
               this.addToBackStack(null);
               this.commit()
           }
           countPage = 1
       }
    }
    private fun updateTransform(){
        val matrix = Matrix()

        // Compute the center of the view finder
        val centerX = view_finder.width / 2f
        val centerY = view_finder.height / 2f

        // Correct preview output to account for display rotation
        val rotationDegrees = when(view_finder.display.rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> return
        }
        matrix.postRotate(-rotationDegrees.toFloat(), centerX, centerY)

        // Finally, apply transformations to our TextureView
        view_finder.setTransform(matrix)
    }

    private class LuminosityAnalyzer : ImageAnalysis.Analyzer {
        private var lastAnalyzedTimestamp = 0L

        /**
         * Helper extension function used to extract a byte array from an
         * image plane buffer
         */
        private fun ByteBuffer.toByteArray(): ByteArray {
            rewind()    // Rewind the buffer to zero
            val data = ByteArray(remaining())
            get(data)   // Copy the buffer into a byte array
            return data // Return the byte array
        }

        override fun analyze(image: ImageProxy, rotationDegrees: Int) {
            val currentTimestamp = System.currentTimeMillis()
            // Calculate the average luma no more often than every second
            if (currentTimestamp - lastAnalyzedTimestamp >=
                TimeUnit.SECONDS.toMillis(1)) {
                // Since format in ImageAnalysis is YUV, image.planes[0]
                // contains the Y (luminance) plane
                val buffer = image.planes[0].buffer
                // Extract image data from callback object
                val data = buffer.toByteArray()
                // Convert the data into an array of pixel values
                val pixels = data.map { it.toInt() and 0xFF }
                // Compute average luminance for the image
                val luma = pixels.average()
                // Log the new luma value
                Log.d("CameraXApp", "Average luminosity: $luma")
                // Update timestamp of last analyzed frame
                lastAnalyzedTimestamp = currentTimestamp
            }
        }
    }
    private fun TTS_fun(){
        //Text To Speech
        mTTS = TextToSpeech(this,TextToSpeech.OnInitListener {status->
            if(status != TextToSpeech.ERROR){
                mTTS!!.language = Locale.UK
            }

        })
       /*var pitch = Settings.Secure.getFloat(contentResolver,Settings.Secure.TTS_DEFAULT_PITCH)
        var speed  = Settings.Secure.getFloat(contentResolver,Settings.Secure.TTS_DEFAULT_RATE)*/
        mTTS!!.setPitch(1.0f)
        mTTS!!.setSpeechRate(1.0f)
    }
    private fun startCamera(){
           // Create configuration object for the viewfinder use case
           val previewConfig = PreviewConfig.Builder().apply {
               setTargetAspectRatio(Rational(1, 1))
               setTargetResolution(Size(MAX_PREVIEW_WIDTH, MAX_PREVIEW_HEIGHT))
           }.build()

           // Build the viewfinder use case
           val preview = Preview(previewConfig)

               // Every time the viewfinder is updated, recompute layout
               preview.setOnPreviewOutputUpdateListener {

                   // To update the SurfaceTexture, we have to remove it and re-add it
                   val parent = view_finder.parent as ViewGroup
                   parent.removeView(view_finder)
                   parent.addView(view_finder, 0)

                   view_finder.surfaceTexture = it.surfaceTexture

                   updateTransform()
               }


           val imageCaptureConfig = ImageCaptureConfig.Builder()
               .apply {
                   setTargetAspectRatio(Rational(1, 1))
                   // We don't set a resolution for image capture; instead, we
                   // select a capture mode which will infer the appropriate
                   // resolution based on aspect ration and requested mode
                   setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
               }.build()

           // Build the image capture use case and attach button click listener
           imageCapture = ImageCapture(imageCaptureConfig)

           val analyzerConfig = ImageAnalysisConfig.Builder().apply {
               // Use a worker thread for image analysis to prevent glitches
               val analyzerThread = HandlerThread(
                   "LuminosityAnalysis"
               ).apply { start() }
               setCallbackHandler(Handler(analyzerThread.looper))
               // In our analysis, we care more about the latest image than
               // analyzing *every* image
               setImageReaderMode(
                   ImageAnalysis.ImageReaderMode.ACQUIRE_LATEST_IMAGE
               )
           }.build()

           // Build the image analysis use case and instantiate our analyzer
           val analyzerUseCase = ImageAnalysis(analyzerConfig).apply {
               analyzer = LuminosityAnalyzer()
           }
           // Bind use cases to lifecycle
           // If Android Studio complains about "this" being not a LifecycleOwner
           // try rebuilding the project or updating the appcompat dependency to
           // version 1.1.0 or higher.

           CameraX.bindToLifecycle(this, preview, imageCapture, analyzerUseCase)

    }
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                view_finder.post { startCamera() }
            } else {
                Toast.makeText(this,
                    "Permissions not granted by the user.",
                    Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }
    private fun allPermissionsGranted()= REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            this, it)== PackageManager.PERMISSION_GRANTED

    }
    override fun ClickCheck(Cclick: Int) {//Interface Check to Take Camera
        CameraCheck = Cclick

       /* Log.d("CameraClick", CameraCheck.toString())
        if (CameraCheck == 1) {
            var externalFile:String = Environment.getExternalStorageDirectory().absolutePath
            val file = File(
                externalFile,"/ChuayArnNa/${dateTime}/"
            )
            if(!file.exists()){
                file.mkdirs()
            }
            var filename =  "${System.currentTimeMillis()}.jpg"
            var fileFloderImage = File(file,filename)
            imageCapture!!.takePicture(fileFloderImage,
                object : ImageCapture.OnImageSavedListener {
                    override fun onError(
                        error: ImageCapture.UseCaseError,
                        message: String, exc: Throwable?
                    ) {
                        val msg = "Photo capture failed: $message"
                        Toast.makeText(this@MainCamera, msg, Toast.LENGTH_SHORT).show()
                        Log.e("CameraXApp", msg)
                        exc?.printStackTrace()
                    }

                    override fun onImageSaved(file: File) {
                        val msg = "Photo capture succeeded: ${file.absolutePath}"
                        Toast.makeText(this@MainCamera, msg, Toast.LENGTH_SHORT).show()
                        Log.d("CameraXApp", msg)
                    }
                })
        }
        //gallery page
        else if(CameraCheck == 3){
            Toast.makeText(this,R.string.Gallery,Toast.LENGTH_LONG).show()
            //val
            val transaction = fragment.beginTransaction()
            container.isVisible = false
            transaction.setCustomAnimations(R.animator.enter,R.animator.pop_enter)
            //transaction.replace(R.id.frame_header,audioquickpage)
            transaction.addToBackStack(null);
            transaction.commit()
        }*/


    }
    override fun setBitmap(): Bitmap {
       if(CameraCheck == 1){
           bitmap = view_finder.getBitmap(140,140)
       }
       else if(CameraCheck == 2){
           bitmap = view_finder.getBitmap()

           mProgressDialog!!.show()
           detectText(bitmap!!)

       }
       return bitmap!!
    }
    /** OCR **/
    fun detectText(bitmap: Bitmap){
        val image = FirebaseVisionImage.fromBitmap(bitmap)
        val detector = FirebaseVision.getInstance()
            .onDeviceTextRecognizer
        val result = detector.processImage(image)
            .addOnSuccessListener { firebaseVisionText ->
                processText(firebaseVisionText)
            }
            .addOnFailureListener {
            }
    }
    fun processText(firebaseVisionText: FirebaseVisionText){
        val resultText =firebaseVisionText.text
        if(resultText.equals("")){
            Log.d(TAG,"Not have text")
            mProgressDialog!!.dismiss()
            Toast.makeText(this,"ไม่พบข้อความบนรูปภาพ",Toast.LENGTH_LONG).show()
        }
        else if(!resultText.equals("")){
            Log.d(TAG,"Converted")

            if(CameraCheck == 1){

            }
            else if(CameraCheck == 2){
                TTS_camera_instance(resultText)
            }

        }

    }
    fun createFolder(){
        var destinationPath = Environment.getExternalStorageDirectory().absolutePath + "/ChauyArnNa_MP3/default/"
        var destination = File(destinationPath)
        if(!destination.exists()){
            destination.mkdirs()
        }
    }
    private fun TTS_camera_instance(result:String){
        myHashRender = HashMap<String,String>()
        myHashRender!!.put(
            TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,
            result)
        val myDir: String = Environment.getExternalStorageDirectory().absolutePath +  "/sounds/"
        val appTmpPath = File(myDir)
        if(!appTmpPath.exists()) {
            appTmpPath.mkdirs()
        }
        Log.d(TAG,myDir)
        //Toast.makeText(activity,myDir,Toast.LENGTH_LONG).show()
        tempFilename = "tmpaudio.mp3"
        tempDestFile = appTmpPath.absolutePath+"/"+tempFilename
        var task = mTTS!!.synthesizeToFile(result,myHashRender,tempDestFile)
        runBlocking {
            val tasking  = async {
                     delay(3000)
                     playMedia(0)
            }
        }
    }
    //play Audio
    private fun playMedia(status:Int){
         mProgressDialog!!.dismiss()
        if(status == 0){
            if(FileCamera.equals("")) {
                val audioPage = AduioNonsavePage()
                val transaction = fragment.beginTransaction().apply {
                    container.isVisible = false
                    this.setCustomAnimations(R.animator.enter, R.animator.pop_enter)
                    this.replace(R.id.frame_header, audioPage)
                    this.addToBackStack(null);
                    this.commit()
                }
            }else {

                val ft = fragment.beginTransaction().apply {
                    container.isVisible = false
                    this?.replace(R.id.frame_header,AduioNonsavePage.newinstant(FileCamera,true))
                    this?.commit()
                }
            }

        }
        else{
            Toast.makeText(this,"ไม่สามารถแปลงไฟล์ได้",Toast.LENGTH_LONG).show()
        }

    }



}

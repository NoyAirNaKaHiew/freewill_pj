package freewill.chauyarnna.CameraModel

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.speech.tts.Voice
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionText
import freewill.chauyarnna.MainActivity
import freewill.chauyarnna.R
import kotlinx.android.synthetic.main.activity_camera_quick_page.view.*
import kotlinx.android.synthetic.main.activity_home_page.view.*
import java.lang.ClassCastException
import java.util.*

class CameraQuickPage : Fragment() {

    private var onTake:CameraInterface? = null
    private var tts: TextToSpeech? = null
    private var voiceobj: Voice? = null
    val texts = "ปัจจุบันคุณอยู่หน้ากล้องถ่ายรูปข้อความ จะประกอบไปด้วยปุ่มทั้งหมด 3 ปุ่มอยู่ตำแหน่งด้านล่างของหน้าจอ\n" +
            "และเมื่อกดถ่ายรูป จะต้องเป็นเวลา 3 วินาทีเพื่อให้กล้องประมวลผลข้อความ และจะทำการเปลี่ยนไปหน้าเล่นเสียงให้อัตโนมัติ\n"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_camera_quick_page, container, false)

        tts = TextToSpeech( activity,TextToSpeech.OnInitListener {this})
        voiceobj = Voice("it-it-x-kda#male_2-local", Locale.forLanguageTag("th")
            ,1,1,false,null)
        tts!!.voice = voiceobj
        tts!!.setSpeechRate(0.8f)
        view.voice_advice.setOnClickListener {

            tts!!.speak(texts,TextToSpeech.QUEUE_FLUSH,null,null)

        }

        view.back2main.setOnClickListener {
            val intent = Intent(activity,MainActivity::class.java)
            startActivity(intent)
            activity!!.finish()
        }
        view.capture_button_quick.setOnClickListener {
            onTake!!.ClickCheck(2)
            onTake!!.setBitmap()


        }



        return view
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        try{
            onTake = context as CameraInterface

        }catch (e: ClassCastException){
            throw ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    override fun onDetach() {
        super.onDetach()
        onTake = null
    }

    override fun onStop() {
        super.onStop()
        try{
            if(tts != null){
                tts!!.stop()
                tts!!.shutdown()
            }
        }catch (e:Exception){

        }

    }

}

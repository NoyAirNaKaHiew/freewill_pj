package freewill.chauyarnna.CameraModel

import android.os.Bundle
import freewill.chauyarnna.R
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.preference.PreferenceManager
import android.util.Base64
import android.view.*
import androidx.fragment.app.Fragment
import freewill.chauyarnna.MainActivity
import kotlinx.android.synthetic.main.activity_camera_page.view.*
import java.io.ByteArrayOutputStream
import java.lang.ClassCastException


class CameraPage() : Fragment(){
    private var onTake:CameraInterface? = null
    var bitmap:Bitmap? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.activity_camera_page, container, false)
        val boas = ByteArrayOutputStream()
        val shre = PreferenceManager.getDefaultSharedPreferences(this.activity)

        view.capture_button.setOnClickListener {
            onTake!!.ClickCheck(1)
            onTake!!.setBitmap().compress(Bitmap.CompressFormat.PNG,100,boas)
            var b = boas.toByteArray()
            var encoded = Base64.encodeToString(b,Base64.DEFAULT)

            val edit = shre.edit()
            edit.putString("image_data",encoded)
            edit.commit()

            convertToBitmap()

        }
        view.voice_advice_camera.setOnClickListener {
          /*  val intent = Intent(this.activity, MainActivity::class.java)
            startActivity(intent)
            this.activity!!.finish()*/
        }
        view.gallery_camera.setOnClickListener {
            onTake!!.ClickCheck(3)
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try{
         onTake = context as CameraInterface

        }catch (e:ClassCastException){
            throw ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    override fun onDetach() {
        super.onDetach()
        onTake = null

    }

    fun convertToBitmap(){
        val shre = PreferenceManager.getDefaultSharedPreferences(this.activity)
        var previouslyEncodedImage = shre.getString("image_data","")
        if(!previouslyEncodedImage.equals("")){
            var b= Base64.decode(previouslyEncodedImage,Base64.DEFAULT)
            bitmap = BitmapFactory.decodeByteArray(b,0,b.size)
            view!!.gallery_camera.setImageBitmap(bitmap)
        }
    }

    //Resume Image Bitmap
    override fun onResume() {
        super.onResume()
        convertToBitmap()
    }
    //Destroy Image Bitmap
    override fun onDestroy() {
        super.onDestroy()
        val shre = PreferenceManager.getDefaultSharedPreferences(this.activity)
        val edit = shre.edit()
        edit.putString("image_data","")
        edit.commit()

    }

}

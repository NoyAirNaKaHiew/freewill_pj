package freewill.chauyarnna.GalleryModel

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.database.MergeCursor
import android.media.Image
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.DisplayMetrics
import android.util.Xml
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import com.bumptech.glide.Glide
import freewill.chauyarnna.Function.GalleryFunction
import freewill.chauyarnna.Function.MapComparator
import freewill.chauyarnna.R
import java.io.File
import java.lang.Exception
import java.security.Permission
import java.util.*
import java.util.jar.Manifest
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

private val REQUEST_PERMISSION_KEY = 1
//var loadAlbumTask:MainGallery.LoadAlbum()
val galleryGridView:GridView? = null
val albumList:ArrayList<HashMap<String,String>>? = null

class MainGallery : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_gallery)

        var iDisplayWidth = resources.displayMetrics.widthPixels
        val resources = applicationContext.resources
        val metrics = resources.displayMetrics
        var dp = iDisplayWidth/ (metrics.densityDpi/160f)

        if(dp < 360){
            dp = (dp-17)/2
            var px = GalleryFunction().convertDpToPixel(dp,applicationContext)
            galleryGridView!!.columnWidth = Math.round(px)
        }
        var permissions = arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE,android.Manifest.permission.READ_EXTERNAL_STORAGE)
        if(!GalleryFunction().hasPermissions(this, permissions.toString())){
            ActivityCompat.requestPermissions(this,permissions,REQUEST_PERMISSION_KEY)
        }

    }
    class LoadAlbum: AsyncTask<String, Void, String>() {
        override fun onPreExecute() {
            super.onPreExecute()
            albumList!!.clear()
        }
        override fun doInBackground(vararg p0: String?): String {
            var xml = ""
            var path = ""
            var album = ""
            var timestamp = ""
            var countPhoto = ""
            val uriExternal = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            val uriInternal = android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI

            var projection = arrayOf(MediaStore.MediaColumns.DATA,
              MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
              MediaStore.MediaColumns.DATE_MODIFIED)

            val cursorExtarnal = MainGallery().contentResolver.query(uriExternal,projection,"_data IS NOT NULL) GROUP BY (bucket_display_name",null,null)
            val cursorInternal = MainGallery().contentResolver.query(uriInternal,projection,"_data IS NOT NULL) GROUP BY (bucket_display_name",null,null)
            var cursorArray: Array<Cursor?> = arrayOf(cursorExtarnal,cursorInternal)
            var cursor = MergeCursor(cursorArray)//cursorArray
            while(cursor!!.moveToNext()){
                path = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA))
                album = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME))
                timestamp = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATE_MODIFIED))
                countPhoto = GalleryFunction().getCount(Activity(), album)
                albumList!!.add(GalleryFunction().mappingInbox(album,path,timestamp,GalleryFunction().converToTime(timestamp),countPhoto))
            }
            cursor.close()
            //Collections.sort(albumList, MapComparator())
            return xml
        }

        override fun onPostExecute(xml: String?) {
            val adapter = AlbumAdapter(MainGallery(), albumList!!)
            galleryGridView!!.adapter = adapter
            galleryGridView!!.onItemClickListener = object:AdapterView.OnItemClickListener{
                override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                      val intent = Intent(MainGallery(),AlbumActivity::class.java)
                      intent.putExtra("name", albumList.get(+position).get(GalleryFunction().KEY_ALBUM))
                      MainGallery().startActivity(intent)

                }

            }
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == REQUEST_PERMISSION_KEY){
            if(grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                 /*   loadAlbumTask = LoadAlbum()
                    loadAlbumTask.execute()*/
            }else{
                Toast.makeText(this@MainGallery,"คุณต้องอุนญาติการเข้าถึง",Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        var permissions = arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE,android.Manifest.permission.READ_EXTERNAL_STORAGE)
        if(!GalleryFunction().hasPermissions(this,permissions.toString())){
            ActivityCompat.requestPermissions(this,permissions, REQUEST_PERMISSION_KEY)
        }else{
        /*   loadAlbumTask = LoadAlbum()
            loadAlbumTask.execute()*/
        }
    }

    class AlbumAdapter(a:Activity,d:ArrayList<HashMap<String,String>>):BaseAdapter(){
        var activity:Activity = a
        var data:ArrayList<HashMap<String,String>> = d

        override fun getView(p0: Int, convertView: View?, parent: ViewGroup?): View? {
            var holder:AlbumViewHolder? = null
            var view:View? = null
            if(convertView == null){
                holder = AlbumViewHolder()
                view  = LayoutInflater.from(activity).inflate(
                    R.layout.album_row, parent, false);

                holder.galleryImage =  view.findViewById<ImageView>(R.id.galleryImage)
                holder.gallery_count = view.findViewById<TextView>(R.id.gallery_count)
                holder.gallery_title = view.findViewById<TextView>(R.id.gallery_count)
                view.tag = holder
            }
            else{
                view = convertView
                holder = view.tag as AlbumViewHolder
            }
            holder.galleryImage!!.id = p0

            var song =  HashMap<String,String>()
            song = data.get(p0)
            try{
              Glide.with(activity!!).load(File(song.get(GalleryFunction().KEY_PATH))).into(holder.galleryImage!!)

            }catch (e:Exception){
            }
            return view
        }

        override fun getItem(p0: Int): Any {
             return p0
        }

        override fun getItemId(p0: Int): Long {
             return p0.toLong()
        }

        override fun getCount(): Int {
             return data.size
        }

    }
    class AlbumViewHolder{
        var galleryImage:ImageView? = null
        var gallery_count:TextView? = null
        var gallery_title:TextView? = null
    }
}

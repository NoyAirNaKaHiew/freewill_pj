package freewill.chauyarnna.GalleryModel

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import freewill.chauyarnna.R

class GalleryPreview : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery_preview)
    }
}
